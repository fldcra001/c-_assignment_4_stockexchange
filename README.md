**The original assignment requirements and instructions can be found in the 'Downloads' section.**

*********************
Author: Craig Feldman 
Date: 	22 May 2014
*********************

=======================
CSC3022H
Assignment 4 - Stock Market 
Readme.txt
========================


----------------
  Instructions
----------------

    1. Compile the application using the 'make' command.
 
    2. Run ./prog -t [value] to start the application. 

Where [value] is the number of trading iterations that the market will perform.
I have assumed that a trading iteration is each market clearance. Hence there are approximately 600 iterations in a 30 second market opening. A good value to use would be about 1000.

eg. ./prog -t 1000	

Initially the starting quantities and prices will be displayed each share.


    3.  Press <enter> to start the simulation.

The simulation starts, and will continue to run without providing details for 30 seconds, or until the required iterations are reached.

At the close of day (30 seconds), the market will print info about how many shares were traded for each company in the last session, as well as the value of these trades. It remains closed for 10 seconds.


    4. When the required number of iterations are reached, the simulation ends. 

The current stock info will be displayed (price and market availability) for each company. A final quantity check will be done which will output the final quantity and initial quantity. These should be equal.

		
    5. Run make clean to remove class files.


--------------------------
  List of important files
--------------------------

driver.cpp			- Starts threads

broker.cpp 			- models a stock broker
market.cpp			- models the market

bid.hpp				- creates bid objects

--------
  Info
--------

* I used Ubuntu on a virtual machine for this project. All code has been tested and compiles and runs as expected on the senior lab pcs.
* 'Geany' was used as the IDE.

* The program runs with 100 broker threads.  

* The makefile generates an executable called 'prog'.

* Note that extensive testing was performed by me to ensure that the program is in fact functioning correctly.

* Priority is given to trades between brokers, in other words, the program tries to first match a buy and sell between two brokers, if a seller does not exist, then it attempts to purchase from the market. Bids and offers are satisfied on a FCFS basis.
 
* Mutexes are used to control access to critical sections.
		  
-------------------
  Troubleshooting
-------------------

 Ensure that all files exist in the same directory.
 This program uses c++ 11 features. So please ensure you have the latest version/compiler.
 

 Enjoy!