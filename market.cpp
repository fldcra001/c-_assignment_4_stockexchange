/*
 * Models the stock market and clears all trades
 * Accepts bids and offers for 0.05 seconds before trading
 * Uses double auctioning to determine stock price 
 * 
 * Craig Feldman
 * 15 May 2014
 * Finalised 22 May 2014
 */
 
 #include "market.hpp"
 #include "helper.hpp"
 #include "bid.hpp"
 
 using namespace fldcra001;
 
 /*
  * Constructor
  * Initialise all stocks to a random quantity and price
  * calculates initial quantity
  */
 market::market() {
	flag = false;
	stocks["amzn"] = std::make_pair(getRandom(100000, 1000000), getRandom(200, 500));
	stocks["goog"] = std::make_pair(getRandom(100000, 1000000), getRandom(200, 500));
	stocks["yhoo"] = std::make_pair(getRandom(100000, 1000000), getRandom(200, 500));
	stocks["msft"] = std::make_pair(getRandom(100000, 1000000), getRandom(200, 500));
	stocks["aapl"] = std::make_pair(getRandom(100000, 1000000), getRandom(200, 500));
	for (auto t : stocks) 
		initialQuantity += t.second.first;
 }
 
/*
 * Starts the market
 * The market loops continuously until the required number of iterations are reached.
 * After 30 seconds, the market closes for 10 seconds and the trade info is displayed
 * A mutex controls access to the bid queue.
 */
void market::run(int iterations){
	using namespace std;
	
	cout << "*Market has opened*" << endl;

	// Loop until required iterations
	for (int i = 1; i<=iterations ; ++i)
	{		
		mu.unlock();
		priceMutex.unlock();
		this_thread::sleep_for(chrono::milliseconds(50));	//accept bids for 0.05 seconds
		
		mu.lock();		// locks the mutex so that no bids may be made while market clears trades

						priceMutex.lock();

			//	cout << "locked " << i << endl;
		if (i == iterations-2) flag = true;

		string name;
		//iterate over all the stocks and every item in queue to determine the price of each stock
		for (auto stock : stocks){
			int p = 0, pb = 0 , nb = 0, sumN = 0, sumPN = 0;
			name = stock.first;

			for (auto it = bids.begin(); it != bids.end();) {
				if ((*it)->getName() != name) {
					++it;
					continue;	
				}

				//dealing with one company at a time
				pb = (*it)->getPrice();
				nb = (*it)->getQuantity();
				sumPN += pb * nb;
				sumN += nb;

				//generate buy and sell queues for a SPECIFIC company
				if ((*it)->getFlag() == true) //buy
					buyQ.push(std::move(*it));
				else
					sellQ.push_back(std::move(*it));	
				
				it = bids.erase(it);	//erase that bid from queue and set iterator to next item
			}

			//factor in the stocks on the market for price calulation
			sumN += stocks[name].first;
			sumPN += stocks[name].first * stocks[name].second;
			
			//done dealing with that one stock, now update price and make trades
			if (sumN != 0)
				p = ceil((sumPN+0.0)/sumN);		//note need to convert it to a double for ceil to work	
			//update stock price to reflect new price
			if (p != 0) {
				stocks[name].second = p;
			}
			
			//go through all the buyers and try make a trade
			while (!buyQ.empty()) {
				bid * b = buyQ.front();
	
				if (b->getPrice() < stocks[name].second) { // bid price to low to purchase
					b->prom->set_value (make_pair(0, 0));
						buyQ.pop();
					continue;
				}

				// go through potential sales if buyer still needs to buy
				while (!sellQ.empty() && b->getTraded() != b->getQuantity()) {
					bid * s = sellQ.front();
					if (s->getPrice() >= stocks[name].second) { //not willing to sell for that price	
						s->prom->set_value (make_pair(s->getTraded(), stocks[name].second));	
						sellQ.pop_front();
						continue;
					}

					// else sell to this buyer if he needs more
					int quantityNeeded = b->getQuantity() - b->getTraded();
					int quantityAvailable = s->getQuantity() - s->getTraded();

					if (quantityNeeded > 0) {
						int sold = (quantityNeeded <= quantityAvailable) ? quantityNeeded : quantityAvailable;
						b->trade(sold);
						s->trade(sold);
						stockTradeInfo[name].first += sold; //add quantity sold
						stockTradeInfo[name].second += sold * stocks[name].second; //add value of trade
						//cout << "buying" << sold << "from seller" << endl;
						
						if (s->getTraded() == s->getQuantity()) { //seller has met demands
							s->prom->set_value (make_pair(s->getTraded(), stocks[name].second));	
							sellQ.pop_front();
						}
					}
				} //done going through sellers

				//gone through all the sellers and buyer still needs to buy, so check if market has stock avaialble
				int quantityNeeded = b->getQuantity() - b->getTraded();
				int quantityAvailable = stocks[name].first;
				int sold = 0;
				if (quantityNeeded > 0 && stocks[name].first > 0) {		
					sold = (quantityNeeded <= quantityAvailable) ? quantityNeeded : quantityAvailable;			
					b->trade(sold);
					stocks[name].first -= sold; //update market
					stockTradeInfo[name].first += sold; //add quantity sold
					stockTradeInfo[name].second += sold * stocks[name].second; //add value of trade
					//	cout << "buying" << sold << "from market" << endl;	
				}

				b->prom->set_value (make_pair(b->getTraded(), stocks[name].second));
				buyQ.pop();					
						
			}	// buy Q empty
		
			//check if partially completed sales in Q
			while (!sellQ.empty()) {
				bid * s = sellQ.front();
				s->prom->set_value (make_pair(s->getTraded(), stocks[s->getName()].second));
				sellQ.pop_front();
			} //sell Q empty
		}
			

		if (i%600 == 0) { //30seconds 
			cout << "*Market closing*\n" << "Market closed for 10 seconds->\n" << endl;
			cout << "Here's the trading info for the last trading session/day:" << endl;
			for (auto info : stockTradeInfo) {
				cout << info.first << " : " << "Shares sold: #" << info.second.first << "\tCummulative sales value: " << info.second.second << "c" << endl; 
				//reset
				info.second.first = 0;
				info.second.second = 0;
			}
			cout << "\nHere's how the current stock market looks:" << endl;
			cout << *this << endl;
			this_thread::sleep_for(chrono::seconds(10));	
			if (!flag) cout << "*Opening market*" << endl;
		}
		mu.unlock();
	}// continuous loop
	
	cout << "*Market closed permanently*\n\n" << endl;
	cout << "\nHere's how the final stock market looks:" << endl;
	cout << *this << endl;

	// Final Check
	//-----------------------------------------------------------
	cout << "\nPerforming final share quantity check..." << endl;
	this_thread::sleep_for(chrono::seconds(5));	//sleep so other threads can add values
	for (auto s : stocks)
		addBrokerQuantity(s.second.first);
	cout << "Final quantity (sum of brokers' portfolios and what remains on the market):\n" << quantityCheck << endl; 
	cout << "Initial quantity:" << endl;
	cout << initialQuantity << endl;
	//-----------------------------------------------------------
}

//=================================================================================================

//return the current price of a stock to a broker
int market::getPrice(std::string s){
	std::lock_guard<std::mutex> lock(priceMutex); //get lock so we can safely get price
	return stocks[s].second;
}
	
//accept a bid
void market::makeBid(bid * b) {
	//wait for mutex
	mu.lock();
	bids.push_back(b);
	mu.unlock();
}


//returns total initial quantity of shares
long market::getInitialQuantity() {
	return initialQuantity;
}

//adds the # of shares in the brokers portfolio to do the final check
void market::addBrokerQuantity(int quantity) {
	std::lock_guard<std::mutex> lock(brokerCheckMutex);
	quantityCheck += quantity;
}

bool market::isClosed() {
	std::lock_guard<std::mutex> lock(mu);
	return flag == true;
}
//=================================================================================================
	
		
	 
