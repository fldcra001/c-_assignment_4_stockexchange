/*
 * Driver for Assignment 4
 * Stock exchange simulation
 * 
 * Craig Feldman
 * 15 May 2014
 * Finalised 22 May 2014
 */
 
 #include <iostream>
 #include <thread>
 
 #include "cmdline_parser.h"
 #include "broker.hpp"
 #include "market.hpp"
 
 int main(int argc, char* argv[]) {
	using namespace std;
	using namespace fldcra001;
	
	//------------------------------------------------------ 
	cmdline_parser parser;
	if(!parser.process_cmdline(argc, argv))
	{
		std::cerr << "Couldn't process command line arguments" << std::endl;
		return 1;
	}

	if(parser.should_print_help()) {
		parser.print_help(std::cout);	
		return 0;
	}
	
	int iterations = parser.get_iterations();	
	//------------------------------------------------------
	
	shared_ptr<market> m(new market());
	cout << "Here are the starting quantities and prices" << endl;
	cout << *m << endl;
	cout << "Total number of initial shares: "<< m->getInitialQuantity() << endl;

	cout << "\nPress <enter> to start the simulation" << endl;
	cin.ignore(); // waits until enter is pressed
	
	const int NUMBER_OF_BROKERS = 200;

	// start up some broker threads
	thread threads[NUMBER_OF_BROKERS];
	for (int i = 0; i < NUMBER_OF_BROKERS; ++i) {	
		threads[i] = thread (&broker::run, new broker(*m));
	}

	//open the market for trading
	thread c(&market::run, m, iterations);

	//join all threads
	for (int i = 0; i < NUMBER_OF_BROKERS; ++i) {	
		threads[i].join();
	}
	c.join();
	
	return 0;
 }

