/*
 * Information about stock bids/offers
 * 
 * Craig Feldman
 * 16 May 2014
 * Finalised 22 May 2014
 */

#ifndef BID_HPP
#define BID_HPP

#include <future>	//std::promise

namespace fldcra001 {
	class bid {
	private:
		std::string * stockName;
		int quantity;
		int price;
		bool buy; //if false then sell
		int traded = 0; // keep track of how many sold/bought so far
		
	public:	
		std::promise<std::pair<int,int> > * prom;	//[Quanity, Price]

		bid (std::string &stock_, int quantity_, int price_, bool buy_, std::promise<std::pair<int,int> > &prom_) :
			stockName(&stock_), quantity(quantity_), price(price_), buy(buy_), prom(&prom_) {};
		
		bid (){};
			
		std::string getName() {return *stockName;}
		int getQuantity() {return quantity;}
		int getPrice() {return price;}
		bool getFlag() {return buy;}
		int getTraded() {return traded;}
		
		void trade(int n) {traded += n;} //add to traded			
			
		// overload <<
		friend std::ostream& operator<<(std::ostream& os, const bid& bd) {
			os << "Stock: " << *bd.stockName << " #" << bd.quantity << " " << bd.price << "c";
			if (bd.buy)
				os << " buy" << std::endl;
			else 
				os << " sell" << std::endl;;
			return os;
		}
	};
}

#endif
