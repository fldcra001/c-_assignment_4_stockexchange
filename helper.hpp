/*
 * Any helper functions for stock exchange
 * Craig Feldman
 * 15 May 2014
 */
 
#ifndef HELPER_HPP_
#define HELPER_HPP_

namespace fldcra001 {
	//generate random int in specified range (inclusive)
	inline int getRandom(int min, int max){
		std::random_device rd; // obtain a random number from hardware
		std::mt19937 eng(rd()); // seed the generator
		std::uniform_int_distribution<> distr(min, max); // define the range
		return distr(eng);
	}
}

#endif
