#ifndef MARKET_HPP
#define MARKET_HPP

#include <map>
#include <iostream>
#include <random>
#include <deque>
#include <queue>         
#include <list>
#include <chrono>
#include <mutex>
#include <atomic>		//flag for when market closes
#include "bid.hpp"

namespace fldcra001 {

class market {
	//name -> [Quantity, Price]
	std::map<std::string, std::pair<int, int> > stocks;
	//share quantity sold and value of sales <quantity, accumulated value>
	std::map<std::string, std::pair<long, long> > stockTradeInfo;

	//initial number of shares
	long initialQuantity = 0;
	//queue of bids (using list so they can be easily removed)
	std::list<bid*> bids;
	
	//queues to keep bids and offers for specific company
	std::deque<bid*> sellQ;
	std::queue<bid*> buyQ;
	
	std::mutex mu;	//control access to bid list
	std::mutex priceMutex;
	std::mutex brokerCheckMutex;
	std::condition_variable cv;

	int quantityCheck = 0;
	
	public:
	std::atomic<bool> flag; //signals market close

	market();	
	void run(int iterations);
	
	friend std::ostream& operator<<(std::ostream& os, const market& mkt) {
		for (auto t : mkt.stocks)
			os <<  " " <<t.first << " #" << t.second.first << " " << t.second.second << "c" << "\n";
		return os;
	}
	
	void makeBid(bid * b);
	int getPrice(std::string s);	
	long getInitialQuantity();	
	void addBrokerQuantity(int quantity);
	bool isClosed();
	
};
}


#endif
