#ifndef BROKER_HPP
#define BROKER_HPP

#include <map>
#include <iostream>
#include "market.hpp"
#include "bid.hpp"
#include <future>


namespace fldcra001 {
	
	class broker {
		int capital = 1000000;	
		//number of shares
		std::map<std::string, int> stocks;
		//the market
		market * mrkt;
		const std::string names[5] = {"aapl", "amzn", "goog", "msft", "yhoo"};

		
	  public:	

		broker(market & m);
		void run(void);
		
		int getCapital() {return capital;}
		
		friend std::ostream& operator<<(std::ostream& os, const broker& br) {
			os << "Broker's portfolio:\n";
			for (auto t : br.stocks)
				os << t.first << " " << t.second << "\n";
			os << "Remaining capital: " << br.capital << "c" << std::endl;
			return os;
		}
		
	};
}
#endif
