# makefile
# CSC 3022H Assignment 4
# Craig Feldman

CC = g++

#  all warnings; c++11; threads
CXXFLAGS= -std=c++0x -Wall -pthread -Wl,--no-as-needed

#boost library
LIBS= -L/usr/lib/ -lboost_program_options

TARGET= prog

OBJECTS= driver.o cmdline_parser.o broker.o market.o

# Linking Rule
$(TARGET): $(OBJECTS) $(HEADERS)
	$(CC) $(OBJECTS) -o $(TARGET) $(LIBS) $(CXXFLAGS) 
#@cp $(TARGET) ./binaries 				#move to binaries

driver.o: driver.cpp
cmdline_parser.o: cmdline_parser.cpp cmdline_parser.h
broker.o: broker.cpp broker.hpp
market.o: market.cpp market.hpp

# Macro to associate *.o with *.cpp
.cpp.o:
	$(CC) $(CXXFLAGS) -c $<

clean:
	@rm -f *.o
