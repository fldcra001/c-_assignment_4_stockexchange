/*
 * Acts a stock broker
 * Randomly chooses to buy or sell a stock at the calculated (random price) and send the bid to the market
 * 
 * Craig Feldman
 * 15 May 2014
 * Finalised 22 May 2014
 */
 
#include "broker.hpp"
#include "helper.hpp"
#include "bid.hpp"

using namespace fldcra001;

//constructor
broker::broker(market & m) {
	stocks["amzn"] = 0;
	stocks["goog"] = 0;
	stocks["yhoo"] = 0;
	stocks["msft"] = 0;
	stocks["aapl"] = 0;
	mrkt = &m;
}

//start broker simulation
void broker::run() {
	using namespace std;

	//loop broker until shutdown signalled
	while (mrkt->isClosed() == false) {
		
		// make a random buy/sell of a particular stock
		bool buy = getRandom(0, 1) == 0 ? false : true; 
		string stockName = names[getRandom(0, 4)];
		int price = mrkt->getPrice(stockName);
	
		if (buy) {		
			double spend = getRandom(0.15*capital, 0.25*capital);
			price = getRandom(int(price*0.95), ceil (price *1.05)); //price +- 5%
			if (price == 0) price += getRandom(10, 50);	
			int quantity = (int) spend/price;

			promise<pair<int,int> > * prom = new promise<pair<int,int> >();			
			bid * b = new bid (stockName, quantity, price, true, *prom);	
			//send the bid to the market
			//cout << "offer to buy @ price " << price <<endl;
			mrkt->makeBid(b);
			future<pair<int,int> > fut = prom->get_future();    // engagement with future
			auto returned = fut.get();
			
			//update the brokers portfolio.
			stocks[stockName] += returned.first;
			capital -= returned.second * returned.first; //cost = p*q
			
			//cout << "future returned value: #" << returned.first << " " << returned.second << "c" << endl; 
			//cout << "after transaction (broker): " << *this << endl; 
			//cout << "after transaction (market): " << *mrkt << endl; 
		} 
		
		//sell
		else {
			int available = stocks[stockName]; //available quantity in portfolio
			if (available > 0) //have stock to sell
			{
				long toSell = getRandom(0.25*available, 0.5*available);

				//make an offer to sell price in range of +-5%
				price = getRandom(int(price*0.95), ceil (price *1.05));
				if (price == 0) price += getRandom(10, 50);
				
				promise<pair<int,int> > * prom = new promise<pair<int,int> >();
				bid * b = new bid (stockName, toSell, price, false, *prom);
				//cout << "offer to sell @ price " << price <<endl;
				//send the offer to the market				
				mrkt->makeBid(b);				
				future<pair<int,int> > fut = prom->get_future();    // engagement with future
				auto returned = fut.get();
					
				//update the brokers portfolio.
				stocks[stockName] -= returned.first;
				capital += returned.second * returned.first; //income = p*q
				
				//cout << "future returned value: #" << returned.first << " " << returned.second << "c" << endl; 
				//cout << "after transaction (broker): " << *this << endl; 
				//cout << "after transaction (market): " << *mrkt << endl; 
			}
		} //sell
	}//loop
		
	//calculate size of portfolio
	int q = 0;
	for (auto s : stocks) {
		q += s.second;
	}
	
	mrkt->addBrokerQuantity(q);
}	

